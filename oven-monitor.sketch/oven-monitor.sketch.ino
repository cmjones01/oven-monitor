/* Arduino Sketch for converting Dallas DS18L20 1-wire temperature sensor
 *  output into a frequency, where 10Hz=1 deg C. Intended for use in
 *  monitoring crystal oven temperature in HP 5245L counter.
 *  
 *  If sensor output is invalid, output is 1Hz to indicate error condition.
 */

#include <OneWire.h>
#include <DallasTemperature.h>
#include <TimerOne.h>

#define ONE_WIRE_BUS 2
#define PWM_PIN 9
#define TEMPERATURE_INVALID(t) (t<-126.0)

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);


void setup() {
  Serial.begin(9600);
  sensors.begin();
  sensors.setResolution(12);

  Timer1.initialize();
}

void loop() {
  float temperature;
  int freq,period;
  
  sensors.requestTemperatures();  

  temperature = sensors.getTempCByIndex(0);
  if(TEMPERATURE_INVALID(temperature)) {
    Serial.println("Invalid");
    Timer1.pwm(PWM_PIN,512,0);
  } else {
    Serial.println(temperature);
    if(temperature>=1.0) {
      freq=(int)((temperature+0.05)*10);
    } else {
      freq = 1;
    }
    period=1000000/freq;
    Timer1.pwm(PWM_PIN,512,period);
  }
  delay(500);
}
